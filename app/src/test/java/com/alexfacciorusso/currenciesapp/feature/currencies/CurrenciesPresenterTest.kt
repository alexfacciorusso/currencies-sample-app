package com.alexfacciorusso.currenciesapp.feature.currencies

import com.alexfacciorusso.currenciesapp.network.CurrenciesService
import com.alexfacciorusso.currenciesapp.rx.Schedulers
import com.alexfacciorusso.currenciesapp.vo.CurrencyResponse
import com.alexfacciorusso.currenciesapp.vo.Entry
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Scheduler
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import java.util.*

class CurrenciesPresenterTest {
    private lateinit var currenciesPresenter: CurrenciesPresenter

    private lateinit var currenciesService: CurrenciesService

    @Before
    fun setUp() {
        val schedulers = object : Schedulers {
            override val main: Scheduler = io.reactivex.schedulers.Schedulers.trampoline()
            override val network: Scheduler = io.reactivex.schedulers.Schedulers.trampoline()
        }

        currenciesService = mock()

        currenciesPresenter = CurrenciesPresenter(schedulers, -1, currenciesService,
                mock())
    }

    @Test
    fun `View taken and updated`() {
        // Given a test response from the network
        val testResponse = CurrencyResponse("EUR", Date(), mapOf("TST" to 0.1, "OTH" to 2.0))
        whenever(currenciesService.getCurrencies(any())).thenReturn(Single.just(
                testResponse))

        // When the view is taken by the presenter
        val view = mock<CurrenciesContract.View>()
        currenciesPresenter.takeView(view)

        // Then the network API gets called with the default currency
        verify(currenciesService).getCurrencies("EUR")

        // And Then the view gets updated with the first entry (no network data yet)
        verify(view).updateEntries(listOf(Entry("EUR", isPrimary = true)))

        // And Then the view gets updated with the entry + network data
        verify(view).updateEntries(listOf(Entry("EUR", isPrimary = true)) +
                testResponse.rates.map { Entry(it.key) }.sortedBy { it.currency })
    }

    @Test
    fun `View updated with first primary entry when changed`() {
        // Given test responses from the network and a view taken by the presenter

        whenever(currenciesService.getCurrencies("EUR")).thenReturn(Single.just(
                CurrencyResponse("EUR", Date(), mapOf("TST" to 0.1, "OTH" to 2.0))))

        val view = mock<CurrenciesContract.View>()
        currenciesPresenter.takeView(view)

        reset(view)
        reset(currenciesService)

        // Given a response for the new TST currency
        whenever(currenciesService.getCurrencies("TST")).thenReturn(Single.just(
                CurrencyResponse("TST", Date(), mapOf("EUR" to 0.3, "OTH" to 2.0))))

        // When a new base entry is set
        currenciesPresenter.baseEntry = Entry("TST", isPrimary = true)

        // Then the view will be updated with the new base entry alone
        verify(view).updateEntries(listOf(Entry("TST", isPrimary = true)))

        // And the network is called for the new currency
        verify(currenciesService).getCurrencies("TST")

        // And then the view will be updated with the currencies returned from the network
        verify(view).updateEntries(listOf(Entry("TST", isPrimary = true),
                Entry("EUR"), Entry("OTH")))

        // When the view gets updated with a new value
        currenciesPresenter.baseEntry = Entry("TST", 3.0)

        // Network is not called
        verifyNoMoreInteractions(currenciesService)

        // And the view is updated with the calculated data
        verify(view).updateEntries(listOf(Entry("TST", 3.0, isPrimary = true),
                Entry("EUR", 3.0 * .3), Entry("OTH", 3 * 2.0)))
    }
}
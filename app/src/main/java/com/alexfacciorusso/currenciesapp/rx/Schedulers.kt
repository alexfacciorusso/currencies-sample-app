package com.alexfacciorusso.currenciesapp.rx

import io.reactivex.Scheduler

/**
 * Interface that abstracts RxJava schedulers.
 */
interface Schedulers {
    /**
     * The scheduler used for the main operations.
     */
    val main: Scheduler

    /**
     * The scheduler used for network purposes.
     */
    val network: Scheduler
}

package com.alexfacciorusso.currenciesapp.rx

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 * The Android [Schedulers] implementation.
 *
 * It uses [AndroidSchedulers.mainThread] as the main scheduler, and [io] as a network one.
 */
class AndroidSchedulersImpl : Schedulers {
    override val main: Scheduler = AndroidSchedulers.mainThread()
    override val network: Scheduler = io.reactivex.schedulers.Schedulers.io()
}
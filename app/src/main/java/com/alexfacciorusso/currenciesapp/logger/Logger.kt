package com.alexfacciorusso.currenciesapp.logger

/**
 * A simple Logger interface.
 */
interface Logger {
    fun debug(tag: String, message: String)
    fun error(tag: String, throwable: Throwable)
}

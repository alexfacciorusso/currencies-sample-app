package com.alexfacciorusso.currenciesapp

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.alexfacciorusso.currenciesapp.feature.currencies.CurrenciesFragment
import kotlinx.android.synthetic.main.activity_main.*

/**
 * The main application Activity. Used only as a [CurrenciesFragment] container.
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)
    }
}

package com.alexfacciorusso.currenciesapp.network

import com.alexfacciorusso.currenciesapp.vo.CurrencyResponse

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Interface for the network service.
 */
interface CurrenciesService {
    @GET("latest")
    fun getCurrencies(@Query("base") baseCurrency: String): Single<CurrencyResponse>
}
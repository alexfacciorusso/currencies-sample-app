package com.alexfacciorusso.currenciesapp

import android.app.Application
import com.alexfacciorusso.currenciesapp.di.appModule
import org.koin.android.ext.android.startKoin

/**
 * The application class. It mainly initialises [org.koin.core.Koin].
 */
class CurrenciesTestApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin(this, listOf(appModule))
    }
}
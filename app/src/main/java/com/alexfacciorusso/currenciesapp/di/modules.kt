package com.alexfacciorusso.currenciesapp.di

import com.alexfacciorusso.currenciesapp.feature.currencies.CurrenciesContract
import com.alexfacciorusso.currenciesapp.feature.currencies.CurrenciesPresenter
import com.alexfacciorusso.currenciesapp.logger.AndroidLogger
import com.alexfacciorusso.currenciesapp.logger.Logger
import com.alexfacciorusso.currenciesapp.network.CurrenciesService
import com.alexfacciorusso.currenciesapp.rx.AndroidSchedulersImpl
import com.alexfacciorusso.currenciesapp.rx.Schedulers
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.*

private const val CURRENCIES_UPDATE_SECONDS = 1L

/**
 * The main dependency injection [org.koin.core.Koin] module.
 */
val appModule = module {
    single<Schedulers> { AndroidSchedulersImpl() }

    single<CurrenciesService> {
        Retrofit.Builder()
                .baseUrl("https://revolut.duckdns.org/")
                .addConverterFactory(MoshiConverterFactory.create(Moshi.Builder()
                        .add(Date::class.java, Rfc3339DateJsonAdapter())
                        .build()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(CurrenciesService::class.java)
    }

    single<Logger> { AndroidLogger() }

    factory<CurrenciesContract.Presenter> { CurrenciesPresenter(get(), CURRENCIES_UPDATE_SECONDS, get(), get()) }
}

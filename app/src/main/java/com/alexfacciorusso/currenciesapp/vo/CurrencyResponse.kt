package com.alexfacciorusso.currenciesapp.vo

import com.squareup.moshi.JsonClass
import java.util.*

@JsonClass(generateAdapter = true)
data class CurrencyResponse(
        val base: String,
        val date: Date,
        val rates: Map<String, Double>
)
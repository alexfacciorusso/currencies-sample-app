package com.alexfacciorusso.currenciesapp.vo

data class Entry(val currency: String, val value: Double = 0.0, val isPrimary: Boolean = false)
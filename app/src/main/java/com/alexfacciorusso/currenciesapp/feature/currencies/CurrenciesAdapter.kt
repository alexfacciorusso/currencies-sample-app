package com.alexfacciorusso.currenciesapp.feature.currencies

import android.content.Context
import android.support.v4.content.ContextCompat.getSystemService
import android.support.v7.recyclerview.extensions.ListAdapter
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import com.alexfacciorusso.currenciesapp.R
import com.alexfacciorusso.currenciesapp.vo.Entry
import kotlinx.android.synthetic.main.item_currency.view.*

class CurrenciesAdapter(private val context: Context) : ListAdapter<Entry, CurrenciesAdapter.ViewHolder>(EntryDiff()) {
    /**
     * Called when a view is clicked.
     */
    var callback: ((Entry) -> Unit)? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currentItem = getItem(position)
        with(holder) {
            currency.text = currentItem.currency

            value.setText("%.2f".format(currentItem.value))

            itemView.setOnClickListener {
                value.requestFocus()

                val imm = getSystemService(context, InputMethodManager::class.java)
                imm?.showSoftInput(value, InputMethodManager.SHOW_IMPLICIT)
                callback?.invoke(getItem(adapterPosition))
            }

            value.onFocusChangeListener = View.OnFocusChangeListener { _, hasFocus ->
                if (hasFocus && adapterPosition >= 0) callback?.invoke(getItem(adapterPosition))
            }

            value.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (getItem(adapterPosition).isPrimary) {
                        callback?.invoke(getItem(adapterPosition)
                                .copy(value = s.toString().toDoubleOrNull() ?: 0.0))
                    }
                }
            })
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_currency, viewGroup, false))
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val currency: TextView = itemView.currency
        val value: EditText = itemView.value

    }
}

class EntryDiff : DiffUtil.ItemCallback<Entry>() {
    override fun areItemsTheSame(p0: Entry, p1: Entry) = p0.currency == p1.currency

    // This workaround is because I don't want to update the primary view
    // because I don't want to lose the caret position when I'm typing in the
    // first one
    override fun areContentsTheSame(p0: Entry, p1: Entry) =
            p0.isPrimary || p1.isPrimary || p0.value == p1.value
}

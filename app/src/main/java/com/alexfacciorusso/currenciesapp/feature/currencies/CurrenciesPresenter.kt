package com.alexfacciorusso.currenciesapp.feature.currencies

import com.alexfacciorusso.currenciesapp.logger.Logger
import com.alexfacciorusso.currenciesapp.network.CurrenciesService
import com.alexfacciorusso.currenciesapp.rx.Schedulers
import com.alexfacciorusso.currenciesapp.vo.Entry
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import java.util.concurrent.TimeUnit

/**
 * The main [CurrenciesContract.Presenter] implementation.
 */
class CurrenciesPresenter(
        private val schedulers: Schedulers,
        private val updateDelaySeconds: Long,
        private val currenciesService: CurrenciesService,
        private val logger: Logger
) : CurrenciesContract.Presenter() {
    override var baseEntry: Entry = Entry("EUR", 0.0, true)
        set(value) {
            field = value.copy(isPrimary = true)
            updateBaseEntry(field)
        }

    private var lastCurrency: String? = null
    private var rates: Map<String, Double>? = null

    private var currencyUpdateDisposable: Disposable? = null

    override fun viewAttached() {
        updateBaseEntry(baseEntry)
    }

    override fun destroy() {
        currencyUpdateDisposable?.dispose()
    }

    private fun updateBaseEntry(baseEntry: Entry) {
        if (lastCurrency == baseEntry.currency && currencyUpdateDisposable?.isDisposed == false) {
            rates?.let { updateEntries() }
            return
        }

        currencyUpdateDisposable?.dispose() // stops any other update in progress

        rates = null
        updateEntries() // refresh the first view anyways

        currencyUpdateDisposable = startFetchingData(baseEntry)
    }

    private fun startFetchingData(baseEntry: Entry): Disposable {
        return currenciesService.getCurrencies(baseEntry.currency)
                .observeOn(schedulers.main)
                .subscribeOn(schedulers.network)
                .toObservable()
                .repeatEvery(updateDelaySeconds, TimeUnit.SECONDS) { updateDelaySeconds > 0 }
                .subscribeBy(onNext = { response ->
                    this.rates = response.rates
                    updateEntries()
                }, onError = {
                    logger.error("Blah", it)
                })
    }

    private fun updateEntries() {
        view?.updateEntries(listOf(baseEntry.copy(isPrimary = true)) +
                rates.orEmpty()
                        .filterKeys { it != baseEntry.currency }
                        .map { Entry(it.key, baseEntry.value * it.value) }
                        .sortedBy { it.currency })

        lastCurrency = baseEntry.currency
    }
}

// Repeats an observable only if the `ifBlock` lambda returns true (optional), else returns itself.
private fun <T> Observable<T>.repeatEvery(
        updateDelaySeconds: Long,
        timeUnit: TimeUnit,
        ifBlock: () -> Boolean = { true }
): Observable<T> {
    return if (ifBlock()) {
        Observable.interval(0, updateDelaySeconds, timeUnit)
                .flatMap { this }
    } else this
}

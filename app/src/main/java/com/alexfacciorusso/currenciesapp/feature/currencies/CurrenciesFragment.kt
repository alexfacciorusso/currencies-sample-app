package com.alexfacciorusso.currenciesapp.feature.currencies


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.alexfacciorusso.currenciesapp.R
import com.alexfacciorusso.currenciesapp.vo.Entry
import kotlinx.android.synthetic.main.fragment_currencies.*
import org.koin.android.ext.android.inject

/**
 * The [CurrenciesContract.View] implementation.
 */
class CurrenciesFragment : Fragment(), CurrenciesContract.View {
    private val presenter by inject<CurrenciesContract.Presenter>()
    private lateinit var adapter: CurrenciesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_currencies, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = CurrenciesAdapter(context!!)
        recycler.layoutManager = LinearLayoutManager(context)
        recycler.adapter = adapter
        adapter.callback = {
            presenter.baseEntry = it
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.takeView(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.dropView()
    }

    override fun updateEntries(entries: List<Entry>) {
        adapter.submitList(entries)
    }
}

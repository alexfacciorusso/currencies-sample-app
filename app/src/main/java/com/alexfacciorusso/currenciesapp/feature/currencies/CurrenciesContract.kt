package com.alexfacciorusso.currenciesapp.feature.currencies

import com.alexfacciorusso.currenciesapp.vo.Entry

interface CurrenciesContract {
    abstract class Presenter {
        protected var view: View? = null
            private set

        abstract var baseEntry: Entry

        /**
         * Called when a View is attached by [takeView].
         */
        protected abstract fun viewAttached()

        /**
         * Called when a View is destroyed by [dropView].
         *
         * Used to clean subscriptions etc.
         */
        protected abstract fun destroy()

        /**
         * Attaches the view to this presenter.
         */
        fun takeView(view: View) {
            this.view = view
            viewAttached()
        }

        /**
         * Drops the view.
         */
        fun dropView() {
            view = null
            destroy()
        }
    }

    interface View {
        fun updateEntries(entries: List<Entry>)
    }
}